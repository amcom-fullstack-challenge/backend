import pytest
from rest_framework.test import APIClient

from apps.customers.models import Customer
from apps.customers.serializers import CustomerSerializer
from apps.sellers.models import Seller
from apps.sellers.serializers import SellerSerializer
from apps.products.models import Product
from apps.products.serializers import ProductSerializer
from apps.sales.models import Sale, SaleItem
from apps.sales.serializers import SaleItemSerializer


@pytest.fixture
def customer_and_seller_data():
    return {'name': 'John Doe', 'phone_number': '1234567890', 'email': 'johndoe@example.com'}


@pytest.fixture
def customer(customer_and_seller_data):
    return Customer.objects.create(**customer_and_seller_data)


@pytest.fixture
def customer_serializer(customer):
    return CustomerSerializer(instance=customer)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def seller(customer_and_seller_data):
    return Seller.objects.create(**customer_and_seller_data)


@pytest.fixture
def seller_serializer(seller):
    return SellerSerializer(instance=seller)


@pytest.fixture
def product_data():
    return {'code': 'cod01', 'name': 'product01', 'description': 'product01 product01',
            'unit_price': 100.00, 'commission_percentage': 5.00}


@pytest.fixture
def product(product_data):
    return Product.objects.create(**product_data)


@pytest.fixture
def product_serializer(product):
    return ProductSerializer(instance=product)


@pytest.fixture
def sale_data(customer, seller):
    return {
        'customer': customer,
        'seller': seller,
        'max_daily_commission': 100.00,
        'min_daily_commission': 5.00,
    }


@pytest.fixture
def sale(sale_data):
    return Sale.objects.create(**sale_data)


@pytest.fixture
def sale_item_data(sale, product):
    return {
        'sale': sale,
        'product': product,
        'quantity': 1,
        'price': 100.00,
        'commission_percentage': 5.00
    }


@pytest.fixture
def sale_item(sale_item_data):
    return SaleItem.objects.create(**sale_item_data)


@pytest.fixture
def sale_item_serializer(sale_item):
    return SaleItemSerializer(instance=sale_item)


@pytest.fixture
def weekly_commission_data():
    return {
        'day_of_week': 0,
        'min_commission': 5.00,
        'max_commission': 100.00,
    }
