PYTEST := py.test -n auto --durations=10
PIP := pip install -r
SECRET_KEY := `bash scripts/generate-secret-key.sh`
PROJECT_NAME := amcon-api
PYTHON_VERSION := 3.10.12
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

.pip:
	pip install pip --upgrade

setup-dev: .pip
	$(PIP) requirements/local.txt

setup-production: .pip
	$(PIP) requirements/production.txt

.clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -f coverage.xml
	rm -fr htmlcov/
	rm -fr test_reports/
	rm -fr reports/
	rm -fr .pytest_cache/

.clean-os:  ## remove os files
	rm -f .DS_store

clean: .clean-build .clean-pyc .clean-test .clean-os  ## remove all build, test, coverage and Python artifacts

.create-venv:
	cd ~/.pyenv; git pull; cd -
	pyenv install -s $(PYTHON_VERSION)
	pyenv uninstall -f $(VENV_NAME)
	pyenv virtualenv $(PYTHON_VERSION) $(VENV_NAME)
	pyenv local $(VENV_NAME)

create-venv: .create-venv setup-dev

# Repository
git-up:
	git pull
	git fetch -p --all

# Database
db-up:
	python manage.py migrate

migrations:
	python manage.py makemigrations $(APP)

migrations-up:
	make migrations APP=customers
	make migrations APP=sellers
	make migrations APP=sales
	make migrations APP=products


code-convention:
	flake8
	pycodestyle

# Docker
docker-push-image:
	docker login registry.gitlab.com
	docker build -t registry.gitlab.com/colmeiacred/database-core .
	docker push registry.gitlab.com/colmeiacred/database-core

test:
	$(PYTEST) --cov-report=term-missing  --cov-report=html --cov-report=xml --cov=.

run:
	DJANGO_READ_DOT_ENV_FILE=on python manage.py runserver 0.0.0.0:8000

generate-apikey:
	DJANGO_READ_DOT_ENV_FILE=on python manage.py apikey default

shell:
	DJANGO_READ_DOT_ENV_FILE=on python manage.py shell

