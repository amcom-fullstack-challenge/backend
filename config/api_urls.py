from django.urls import path, include

urlpatterns = [
    path('', include('apps.customers.urls')),
    path('', include('apps.sellers.urls')),
    path('', include('apps.products.urls')),
    path('', include('apps.sales.urls')),
]