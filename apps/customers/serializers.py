from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Customer

from django.utils.translation import gettext_lazy as _


class CustomerSerializer(serializers.ModelSerializer):
    name = serializers.CharField(
        error_messages={
            'required': _('Please provide a username.'),
        }
    )
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=Customer.objects.all(),
                                    message=_("An customer with this email already exists."))]
    )

    class Meta:
        model = Customer
        fields = ['id', 'name', 'phone_number', 'email']
