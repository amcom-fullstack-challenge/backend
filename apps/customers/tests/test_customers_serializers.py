import pytest

from apps.customers.models import Customer
from apps.customers.serializers import CustomerSerializer


@pytest.mark.django_db
def test_contains_expected_fields(customer_serializer, customer_and_seller_data):
    data = customer_serializer.data
    assert set(data.keys()) == {'id', 'name', 'phone_number', 'email'}


@pytest.mark.django_db
def test_validation(customer, customer_and_seller_data):
    serializer = CustomerSerializer(
        data={'name': 'Jane Doe', 'phone_number': '0987654321', 'email': customer_and_seller_data['email']})
    assert not serializer.is_valid()
    assert set(serializer.errors.keys()) == {'email'}
    assert serializer.errors['email'][0] == "An customer with this email already exists."


@pytest.mark.django_db
def test_custom_error_message():
    serializer = CustomerSerializer(data={'phone_number': '0987654321', 'email': 'janedoe@example.com'})
    assert not serializer.is_valid()
    assert serializer.errors['name'][0] == 'Please provide a username.'
