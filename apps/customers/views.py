from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from common.mixins import CustomResponseMixin
from .models import Customer
from .serializers import CustomerSerializer


class CustomerViewSet(CustomResponseMixin, viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
