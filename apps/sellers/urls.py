from rest_framework.routers import DefaultRouter
from .views import SellerViewSet

router = DefaultRouter()
router.register(r'sellers', SellerViewSet)

urlpatterns = router.urls
