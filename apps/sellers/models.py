from django.db import models
from django_extensions.db.models import TimeStampedModel
import uuid


class Seller(TimeStampedModel):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  name = models.CharField(max_length=200, null=True)
  phone_number = models.CharField(max_length=16)
  email = models.EmailField(blank=True, unique=True)

  def __str__(self):
    return self.name

  class Meta:
    db_table = 'sellers'
    verbose_name = 'seller'
    verbose_name_plural = 'sellers'
    