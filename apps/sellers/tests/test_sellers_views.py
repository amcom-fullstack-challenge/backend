import pytest
from rest_framework.test import APIClient
from rest_framework import status
from apps.customers.models import Customer

RESOURCE_PATH = '/api/v1/customers/'


@pytest.mark.django_db
def test_customer_list(api_client, customer):
    response = api_client.get(RESOURCE_PATH)
    assert response.status_code == status.HTTP_200_OK
    assert response.data['error'] is False


@pytest.mark.django_db
def test_customer_detail(api_client, customer):
    response = api_client.get(f'{RESOURCE_PATH}{customer.id}/')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['data']['name'] == customer.name


@pytest.mark.django_db
def test_create_customer(api_client, customer_and_seller_data):
    response = api_client.post(RESOURCE_PATH, customer_and_seller_data)
    assert response.status_code == status.HTTP_201_CREATED
    assert Customer.objects.count() == 1


@pytest.mark.django_db
def test_update_customer(customer_and_seller_data, api_client, customer):
    update_data = customer_and_seller_data
    update_data['name'] = 'Jane Doe'
    response = api_client.put(f'{RESOURCE_PATH}{customer.id}/', update_data)
    customer.refresh_from_db()
    assert response.status_code == status.HTTP_200_OK
    assert customer.name == 'Jane Doe'


@pytest.mark.django_db
def test_delete_customer(api_client, customer):
    assert Customer.objects.count() == 1
    response = api_client.delete(f'{RESOURCE_PATH}{customer.id}/')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Customer.objects.count() == 0
