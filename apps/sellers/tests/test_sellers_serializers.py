import pytest

from apps.sellers.serializers import SellerSerializer


@pytest.mark.django_db
def test_contains_expected_fields(seller_serializer):
    data = seller_serializer.data
    assert set(data.keys()) == {'id', 'name', 'phone_number', 'email'}


@pytest.mark.django_db
def test_validation(seller, customer_and_seller_data):
    serializer = SellerSerializer(
        data={'name': 'Jane Doe', 'phone_number': '0987654321', 'email': customer_and_seller_data['email']})
    assert not serializer.is_valid()
    assert set(serializer.errors.keys()) == {'email'}
    assert serializer.errors['email'][0] == "An seller with this email already exists."


@pytest.mark.django_db
def test_custom_error_message():
    serializer = SellerSerializer(data={'phone_number': '0987654321', 'email': 'janedoe@example.com'})
    assert not serializer.is_valid()
    assert serializer.errors['name'][0] == 'Please provide a username.'
