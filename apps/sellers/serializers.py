from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Seller

from django.utils.translation import gettext_lazy as _


class SellerSerializer(serializers.ModelSerializer):
  name = serializers.CharField(
    error_messages={
        'required': _('Please provide a username.'),
    }
  )
  email = serializers.EmailField(
    validators=[UniqueValidator(queryset=Seller.objects.all(),
    message=_("An seller with this email already exists."))]
  )
  
  class Meta:
    model = Seller
    fields = ['id', 'name', 'phone_number', 'email']
