from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from common.mixins import CustomResponseMixin
from .models import Seller
from .serializers import SellerSerializer


class SellerViewSet(CustomResponseMixin, viewsets.ModelViewSet):
  queryset = Seller.objects.all()
  serializer_class = SellerSerializer
