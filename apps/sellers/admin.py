from django.contrib import admin
from .models import Seller


class SellerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone_number', 'email')
    search_fields = ('name', 'email', 'phone_number')
    list_filter = ('created', 'modified')
    ordering = ('name',)
    readonly_fields = ('id',)


admin.site.register(Seller, SellerAdmin)
