import uuid
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.core.validators import MinValueValidator, MaxValueValidator


class Product(TimeStampedModel):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  code = models.CharField(max_length=100, unique=True)
  name = models.CharField(max_length=255)
  description = models.TextField()
  unit_price = models.DecimalField(max_digits=10, decimal_places=2)
  commission_percentage = models.DecimalField(
    max_digits=5, 
    decimal_places=2,
    validators=[
      MinValueValidator(0),
      MaxValueValidator(10)
    ]
  )
  
  def __str__(self):
    return self.name
  
  class Meta:
    db_table = 'products'
    verbose_name = 'product'
    verbose_name_plural = 'products'
