from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from common.mixins import CustomResponseMixin
from .models import Product
from .serializers import ProductSerializer


class ProductsViewSet(CustomResponseMixin, viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
