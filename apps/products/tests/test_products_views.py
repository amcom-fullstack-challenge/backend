import pytest
from rest_framework import status
from apps.products.models import Product

RESOURCE_PATH = '/api/v1/products/'


@pytest.mark.django_db
def test_product_list(api_client):
    response = api_client.get(RESOURCE_PATH)
    assert response.status_code == status.HTTP_200_OK
    assert response.data['error'] is False


@pytest.mark.django_db
def test_product_detail(api_client, product):
    response = api_client.get(f'{RESOURCE_PATH}{product.id}/')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['data']['name'] == product.name


@pytest.mark.django_db
def test_create_product(api_client, product_data):
    response = api_client.post(RESOURCE_PATH, product_data)
    assert response.status_code == status.HTTP_201_CREATED
    assert Product.objects.count() == 1


@pytest.mark.django_db
def test_update_product(product_data, api_client, product):
    update_data = product_data
    update_data['name'] = 'product02'
    response = api_client.put(f'{RESOURCE_PATH}{product.id}/', update_data)
    product.refresh_from_db()
    assert response.status_code == status.HTTP_200_OK
    assert product.name == 'product02'


@pytest.mark.django_db
def test_delete_product(api_client, product):
    assert Product.objects.count() == 1
    response = api_client.delete(f'{RESOURCE_PATH}{product.id}/')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Product.objects.count() == 0
