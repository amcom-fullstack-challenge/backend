import pytest

from apps.products.serializers import ProductSerializer


@pytest.mark.django_db
def test_contains_expected_fields(product_serializer, product_data):
    data = product_serializer.data
    assert set(data.keys()) == {'id', 'code', 'name', 'description', 'unit_price', 'commission_percentage'}


@pytest.mark.django_db
def test_validation(product, product_data):
    serializer = ProductSerializer(
        data={'code': product_data['code'], 'name': 'product011', 'description': 'product011 product011',
              'unit_price': 50.00, 'commission_percentage': 2.00})
    assert not serializer.is_valid()
    assert set(serializer.errors.keys()) == {'code'}
    assert serializer.errors['code'][0] == "An Products with this code already exists."

