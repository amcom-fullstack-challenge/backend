from django.contrib import admin
from .models import Product


class ProductAdmin(admin.ModelAdmin):
  list_display = ('code', 'name', 'unit_price', 'commission_percentage')
  search_fields = ('code', 'name')
  list_filter = ('created', 'modified')
  ordering = ('-created',)

admin.site.register(Product, ProductAdmin)
