from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Product

from django.utils.translation import gettext_lazy as _


class ProductSerializer(serializers.ModelSerializer):
    code = serializers.CharField(
        validators=[UniqueValidator(queryset=Product.objects.all(),
                                    message=_("An Products with this code already exists."))]
    )

    class Meta:
        model = Product
        fields = ['id', 'code', 'name', 'description', 'unit_price', 'commission_percentage']
