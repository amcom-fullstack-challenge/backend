import uuid
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django_extensions.db.models import TimeStampedModel

from apps.customers.models import Customer
from apps.products.models import Product
from apps.sellers.models import Seller

DAYS_OF_WEEK = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
DAY_CHOICES = [(day, day) for day in DAYS_OF_WEEK]


class Sale(TimeStampedModel):
    invoice_number = models.AutoField(primary_key=True, editable=False, verbose_name="Invoice Number")
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, verbose_name="Customer")
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE, verbose_name="Seller")
    min_daily_commission = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
        ],
        default=0.00,
        verbose_name='Min Daily Commission'
    )
    max_daily_commission = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
        ],
        default=0.00,
        verbose_name='Max Daily Commission'
    )
    total_amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(1000000)
        ],
        default=0.00,
        verbose_name='Total'
    )
    discount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(1000000)
        ],
        default=0.00,
        verbose_name='Discount'
    )

    class Meta:
        db_table = 'sales'
        verbose_name = 'sale'
        verbose_name_plural = 'sales'


class SaleItem(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sale = models.ForeignKey(Sale, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    commission_percentage = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(10)
        ],
        default=0.00
    )

    class Meta:
        db_table = 'sale_items'
        verbose_name = 'sale item'
        verbose_name_plural = 'sale items'


class WeeklyCommission(TimeStampedModel):

    day_of_week = models.CharField(max_length=9, choices=DAY_CHOICES, unique=True)
    min_commission = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
        ]
    )
    max_commission = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100)
        ]
    )

    class Meta:
        db_table = 'weekly_commission'
        verbose_name = 'weekly commission'
        verbose_name_plural = 'weekly commissions'
