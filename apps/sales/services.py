# services.py
from .models import WeeklyCommission, DAYS_OF_WEEK
import datetime

from ..products.models import Product


def get_min_commission_for_today():
    today = datetime.date.today()
    day_name = DAYS_OF_WEEK[today.weekday()]
    weekly_commission = WeeklyCommission.objects.filter(day_of_week=day_name).first()
    return weekly_commission.min_commission if weekly_commission else None


def get_max_commission_for_today():
    today = datetime.date.today()
    day_name = DAYS_OF_WEEK[today.weekday()]
    weekly_commission = WeeklyCommission.objects.filter(day_of_week=day_name).first()
    return weekly_commission.max_commission if weekly_commission else None


def get_commission_for_product(product_id, min_commission, max_commission):
    product = Product.objects.get(pk=product_id)
    if min_commission <= product.commission_percentage <= max_commission:
        commission = product.commission_percentage
    else:
        if product.commission_percentage < min_commission:
            commission = min_commission
        else:
            commission = max_commission
    return commission
