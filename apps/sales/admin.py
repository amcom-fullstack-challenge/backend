from django.contrib import admin
from .models import WeeklyCommission, Sale


class SalesAdmin(admin.ModelAdmin):
    list_display = ('invoice_number', 'seller', 'customer', 'min_daily_commission', 'max_daily_commission',
                    'total_amount', 'discount')
    list_filter = ('created', 'modified')
    ordering = ('invoice_number',)
    readonly_fields = ('invoice_number',)


class WeeklyCommissionAdmin(admin.ModelAdmin):
    list_display = ('day_of_week', 'min_commission', 'max_commission')
    list_filter = ('created', 'modified')
    ordering = ('day_of_week',)


admin.site.register(Sale, SalesAdmin)
admin.site.register(WeeklyCommission, WeeklyCommissionAdmin)
