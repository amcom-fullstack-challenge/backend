from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from django.db import transaction

from common.mixins import CustomResponseMixin
from .models import Sale
from .serializers import SaleItemSerializer, SaleSerializer
from .services import get_commission_for_product


class SalesViewSet(CustomResponseMixin, viewsets.ModelViewSet):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        items_data = data.pop('items', None)

        sale_serializer = self.get_serializer(data=data)
        sale_serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            sale = sale_serializer.save()

            if items_data:
                min_commission = sale.min_daily_commission
                max_commission = sale.max_daily_commission
                for item in items_data:
                    item['sale'] = sale.pk
                    commission_percentage = get_commission_for_product(
                        item.get('product'), min_commission, max_commission)
                    item['commission_percentage'] = commission_percentage

                items_serializer = SaleItemSerializer(data=items_data, many=True)

                if not items_serializer.is_valid():
                    raise ValidationError(items_serializer.errors)

                items_serializer.save()

            return Response(sale_serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        sale = self.get_object()
        data = request.data
        items_data = data.pop('items', None)

        sale_serializer = self.get_serializer(sale, data=data, partial=True)
        sale_serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            sale_serializer.save()

            if items_data:
                for item_data in items_data:
                    item = sale.items.filter(id=item_data.get('id')).first()

                    if item:
                        item_serializer = SaleItemSerializer(item, data=item_data, partial=True)
                    else:
                        item_data['sale'] = sale.pk
                        item_serializer = SaleItemSerializer(data=item_data)

                    item_serializer.is_valid(raise_exception=True)
                    item_serializer.save()

        return Response(sale_serializer.data, status=status.HTTP_200_OK)
