import pytest

from apps.sales.serializers import SaleSerializer


@pytest.mark.django_db
def test_sale_serializer_validates_max_min_commission():
    data = {'max_daily_commission': 110, 'min_daily_commission': -5}
    serializer = SaleSerializer(data=data)

    assert not serializer.is_valid()


@pytest.mark.django_db
def test_sale_serializer_serializes_sale_data(sale):
    serializer = SaleSerializer(sale)
    assert set(serializer.data.keys()) == {'invoice_number', 'items', 'created', 'seller', 'customer'}


