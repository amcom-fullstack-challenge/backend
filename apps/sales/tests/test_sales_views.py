import pytest
from rest_framework.test import APIClient
from rest_framework import status

from apps.sales.models import Sale, SaleItem

RESOURCE_PATH = '/api/v1/sales/'


@pytest.mark.django_db
def test_product_list(api_client):
    response = api_client.get(RESOURCE_PATH)
    assert response.status_code == status.HTTP_200_OK
    assert response.data['error'] is False


@pytest.mark.django_db
def test_product_detail(api_client, create_product):
    response = api_client.get(f'{RESOURCE_PATH}{create_product.id}/')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['data']['name'] == create_product.name


@pytest.mark.django_db
def test_create_sale(api_client, customer, seller, product):

    data = {
        "invoice_number": "inv01",
        "customer": customer.pk,
        "seller": seller.pk,
        "max_daily_commission": 100.00,
        "min_daily_commission": 5.00,
        "items": [
            {
                "product": product.pk,
                "quantity": 1,
                "price": 100.00,
                "commission_percentage": 5.00
            },
        ],
    }

    response = api_client.post(RESOURCE_PATH, data, format='json')

    assert response.status_code == status.HTTP_201_CREATED

    assert Sale.objects.filter(invoice_number="inv01").exists()

    sale = Sale.objects.get(invoice_number="inv01")
    assert SaleItem.objects.filter(sale=sale).count() == len(data["items"])


@pytest.mark.django_db
def test_update_sale(api_client, sale, sale_item):
    item_id = sale_item.id
    update_data = {
        'max_daily_commission': 8.00,
        'min_daily_commission': 1.00,
        'items': [
            {
                'id': item_id,
                'product': sale_item.product_id,
                'quantity': 2,
                'price': 110.00,
                'commission_percentage': 6.00
            }
        ]
    }

    response = api_client.put(f'{RESOURCE_PATH}{sale.invoice_number}/', update_data, format='json')

    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_delete_sale(api_client, sale):
    assert Sale.objects.count() == 1
    response = api_client.delete(f'{RESOURCE_PATH}{sale.invoice_number}/')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Sale.objects.count() == 0
