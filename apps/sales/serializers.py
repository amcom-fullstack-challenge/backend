from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from .models import Sale, SaleItem, WeeklyCommission

from rest_flex_fields import FlexFieldsModelSerializer

from .services import get_min_commission_for_today, get_max_commission_for_today
from ..products.models import Product
from ..products.serializers import ProductSerializer


class SaleItemSerializer(FlexFieldsModelSerializer):
    product = PrimaryKeyRelatedField(queryset=Product.objects.all())
    commission_percentage = serializers.DecimalField(max_digits=5, decimal_places=2, max_value=100,
                                                     min_value=0, required=False)

    class Meta:
        model = SaleItem
        fields = ['sale', 'product', 'quantity', 'price', 'commission_percentage']
        expandable_fields = {
            'sale': 'apps.sales.serializers.SaleSerializer',
            'product': 'apps.products.serializers.ProductSerializer',
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['product'] = ProductSerializer(instance.product).data
        return data

    def to_internal_value(self, data):
        product_id = data.get('product')
        if isinstance(product_id, dict):
            product_id = product_id.get('id')
        product = Product.objects.get(pk=product_id)
        validated_data = super().to_internal_value(data)
        validated_data['product'] = product
        return validated_data


class SaleSerializer(FlexFieldsModelSerializer):
    max_daily_commission = serializers.DecimalField(write_only=True, max_digits=5, decimal_places=2, max_value=100,
                                                    min_value=0, required=False)
    min_daily_commission = serializers.DecimalField(write_only=True, max_digits=5, decimal_places=2, max_value=100,
                                                    min_value=0, required=False)
    items = SaleItemSerializer(many=True, required=False)

    class Meta:
        model = Sale
        fields = ['invoice_number', 'created', 'customer', 'seller', 'max_daily_commission', 'min_daily_commission',
                  'items', 'total_amount', 'discount']
        expandable_fields = {
            'customer': 'apps.customers.serializers.CustomerSerializer',
            'seller': 'apps.sellers.serializers.SellerSerializer',
        }

    def create(self, validated_data):
        min_commission = get_min_commission_for_today()
        max_commission = get_max_commission_for_today()
        validated_data['min_daily_commission'] = min_commission
        validated_data['max_daily_commission'] = max_commission
        return super().create(validated_data)


class WeeklyCommissionSerializer(serializers.ModelSerializer):
    min_commission = serializers.DecimalField(max_digits=5, decimal_places=2, max_value=100, min_value=0)
    max_commission = serializers.DecimalField(max_digits=5, decimal_places=2, max_value=100, min_value=0)

    class Meta:
        model = WeeklyCommission
        fields = ['day_of_week', 'min_commission', 'max_commission']
