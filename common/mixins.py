from rest_framework.response import Response

class CustomResponseMixin:
  def finalize_response(self, request, response, *args, **kwargs):
    response.data = {
      "error": False if response.status_code < 400 else True,
      "data": response.data
    }
    return super().finalize_response(request, response, *args, **kwargs)
