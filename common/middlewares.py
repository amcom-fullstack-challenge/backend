class CustomResponseMiddleware:
  def __init__(self, get_response):
    self.get_response = get_response

  def __call__(self, request):
    response = self.get_response(request)
    if 'api' in request.path:
      response.data = {
        'message': 'Request was successful',
        'data': response.data
      }
    return response
