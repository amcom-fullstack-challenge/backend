# Config and run project

## Project structure
    |--docker
        |--docker-compose.yaml
    |--backend
    |--frontend

## Clone the repositories

* docker
* backend
* frontend

## Run the project docker file

You need this in the docker folder to run the project.

    cd docker
    docker-compose build
    docker-compose up -d

## URLs

- Frontend: http://localhost:3000/
- Backend: http://localhost:8000/


## User Default

    admin:admin
